/*
   Copyright (c) 2020-2021 Laurent Montel <montel@kde.org>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#pragma once

#include <QWidget>

#include "libruqolawidgets_private_export.h"
class QTabWidget;
class QLineEdit;
class QListView;
class RocketChatAccount;
class EmoticonRecentUsedFilterProxyModel;
class EmoticonModelFilterProxyModel;
class EmoticonCustomModelFilterProxyModel;
class RecentUsedEmoticonView;
class EmoticonCategoryModelFilterProxyModel;
class LIBRUQOLAWIDGETS_TESTS_EXPORT EmoticonMenuWidget : public QWidget
{
    Q_OBJECT
public:
    explicit EmoticonMenuWidget(QWidget *parent = nullptr);
    ~EmoticonMenuWidget() override;

    void setCurrentRocketChatAccount(RocketChatAccount *account);
    void loadRecentUsed();
Q_SIGNALS:
    void insertEmoticons(const QString &emoticon);

private:
    void slotInsertEmoticons(const QString &identifier);
    void initializeTab(RocketChatAccount *account);

    QLineEdit *const mSearchLineEdit;
    QTabWidget *const mTabWidget;
    EmoticonRecentUsedFilterProxyModel *const mRecentUsedFilterProxyModel;
    EmoticonModelFilterProxyModel *const mEmoticonFilterProxyModel;
    EmoticonCustomModelFilterProxyModel *const mEmoticonCustomFilterProxyModel;
    QListView *const mSearchEmojisView;
    RecentUsedEmoticonView *const mRecentUsedEmoticonView;
    QListView *const mCustomEmojiView;
    QList<EmoticonCategoryModelFilterProxyModel *> mEmoticonListViews;
    int mAllTabIndex = -1;
};

